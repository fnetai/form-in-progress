import React from 'react';
import Layout from "@fnet/react-layout-asya";

import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';

export default (props) => {
    return (
        <Layout {...props}>
            <Box sx={{ width: '100%' }}>
                <LinearProgress />
            </Box>
        </Layout>
    );
}